# Cheat sheet for protractor tests

From bash:

```
$ node --version
v12.18.3

$ npm --version
6.14.6

$ npm install
$ npm test # Execute test program

$ echo "Go to Visual Studio Code, open this folder, set breakpoints in runOrder.js and Start Debugging / press F5."
$ echo "Have fun!"
```

# TODO

* Add example why forAll is bad and for ... of should be used.


