// https://gist.github.com/tamlyn/dd6d54cdbe6eccd51a4bc61f5844bec4

/**
 * Demonstrate execution order of code in Jest/Jasmine
 */
function resolveAfter2Seconds(message) {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(message)
      }, Math.ceil(Math.random() * 500))
    })
  }
  
  async function promiseLog(message) {
    const result = await resolveAfter2Seconds(message)
    console.log(result)
  }
  
  describe('First test', () => {
    beforeAll(async (done) => {
      await promiseLog('Before all')
      done()
    })
    afterAll(async (done) => {
      await promiseLog('After all')
      done()
    })
    beforeEach(async (done) => {
      await promiseLog('Before each')
      done()
    })
    afterEach(async (done) => {
      await promiseLog('After each')
      done()
    })
  
    describe('Inner', () => {
      beforeAll(async (done) => {
        await promiseLog('Before all inner')
        done()
      })
      afterAll(async (done) => {
        await promiseLog('After all inner')
        done()
      })
      beforeEach(async (done) => {
        await promiseLog('Before each inner')
        done()
      })
      afterEach(async (done) => {
        await promiseLog('After each inner')
        done()
      })
  
      it('runs a test', async () => {
        await promiseLog('>>> Running first test')
      })
      it('runs another test', async () => {
        await promiseLog('>>> Running second test')
      })
    })
    it('runs this test too', async () => {
      await promiseLog('>>> Running third test')
    })
  })
  
  describe('Second test', () => {
    // await promiseLog("Can't use await outside an async function.");
    it.skip("doesn't run this test", async () => {
      await promiseLog('Nope')
    })
  })
